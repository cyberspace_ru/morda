import * as React from "react";
import {AppState} from "../behaviour/AppState";
import {connect, Dispatch} from "react-redux";
import AppBar from "@material-ui/core/AppBar/AppBar";
import Select from "@material-ui/core/Select/Select";
import ListItemText from "@material-ui/core/ListItemText/ListItemText";
import Checkbox from "@material-ui/core/Checkbox/Checkbox";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import Input from "@material-ui/core/Input/Input";
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import {THEME} from "../constants";
import Toolbar from "@material-ui/core/Toolbar/Toolbar";
import TextField from "@material-ui/core/TextField/TextField";
import {Request} from "../network/Request";
import Currencies from "../network/Currencies";
import Typography from "@material-ui/core/Typography/Typography";
import {Actions, AppAction} from "../behaviour/actions";
import * as Autosuggest from "react-autosuggest";
import {SuggestionsFetchRequestedParams} from "react-autosuggest";
import {InputProps} from "react-autosuggest";
import {RenderSuggestionParams} from "react-autosuggest";
import {ChangeEvent} from "react-autosuggest";
import {RenderSuggestionsContainerParams} from "react-autosuggest";
import SteamApp from "../network/SteamApp";
import AssetsResult from "../network/AssetsResult";

interface ConverterAppProps {
    currencies?: Currencies
    onLoadCurrencies?: (currencies: Currencies) => void
}

interface ConverterAppState {
    loading: boolean
    appValue: string
    app?: SteamApp
    result?: AssetsResult
    suggestions: SteamApp[]
    selectedCurrencies: string[]
    selectedCryptocurrency: string
}

class ConverterApp extends React.Component<ConverterAppProps, ConverterAppState> {
    constructor(props: any, context?: any) {
        super(props, context);
        this.state = {
            loading: false,
            appValue: "",
            suggestions: [],
            selectedCurrencies: [],
            selectedCryptocurrency: ""
        };
        this.onSelectCurrency = this.onSelectCurrency.bind(this);
        this.onSelectCryptocurrency = this.onSelectCryptocurrency.bind(this);
        this.onAppChange = this.onAppChange.bind(this);
        this.onGetSuggestionValue = this.onGetSuggestionValue.bind(this);
        this.onRenderSuggestion = this.onRenderSuggestion.bind(this);
        this.onRenderInputComponent = this.onRenderInputComponent.bind(this);
        this.handleSuggestionsFetchRequested = this.handleSuggestionsFetchRequested.bind(this);
        this.onSuggestionsClearRequested = this.onSuggestionsClearRequested.bind(this);
        this.onRenderSuggestionsContainer = this.onRenderSuggestionsContainer.bind(this);
    }

    public render() {
        return (
            <MuiThemeProvider theme={THEME}>
                <AppBar position={"static"}>
                    <Toolbar>
                        {this.props.currencies ? this.renderInputs(this.props.currencies) : this.renderLoadingStub()}
                    </Toolbar>
                </AppBar>
                <div>
                    {this.renderApp(this.state.result)}
                </div>
            </MuiThemeProvider>
        )
    }

    private renderApp(assets?: AssetsResult): JSX.Element[] {
        const result: JSX.Element[] = [];
        if (assets) {
            for (const asset of assets.result) {
                const ps = [];
                for (const key of Array.from(asset.prices.keys())) {
                    ps.push(asset.prices.get(key) + " " + key)
                }
                result.push(
                    <MenuItem key={asset.classid}
                              value={asset.name}>
                        <ListItemText primary={asset.name} secondary={this.state.selectedCryptocurrency + ": " + ps.join(", ")}/>
                    </MenuItem>
                )
            }
        }
        return result;
    }

    private renderLoadingStub() {
        if (!this.state.loading) {
            this.setState({
                ...this.state,
                loading: true
            });
            Request.getCurrencies((r, cur) => {
                if (this.props.onLoadCurrencies && cur) {
                    this.props.onLoadCurrencies(cur);
                }
            });
        }
        return <Typography>Loading...</Typography>
    }

    private renderInputs(currencies: Currencies) {
        const selectedCurrency = () => this.state.selectedCurrencies.join(', ');
        const selectedCryptocurrency = () => this.state.selectedCryptocurrency;
        const currencyToSelect: string[] = [];
        const cryptocurrencyToSelect: string[] = [];
        for (const key of Array.from(currencies.currencies.keys())) {
            currencyToSelect.push(key);
            const value = currencies.currencies.get(key);
            if (value && this.state.selectedCurrencies.indexOf(key) > -1) {
                for (const pair of value) {
                    if (cryptocurrencyToSelect.indexOf(pair) === -1) {
                        cryptocurrencyToSelect.push(pair)
                    }
                }
            }
        }
        const AppsAutosuggest = Autosuggest as { new(): Autosuggest<string> };
        const inputProps = {
            placeholder: "Type app",
            value: this.state.appValue,
            onChange: this.onAppChange
        };
        return (
            <div>
                <div>
                    <AppsAutosuggest inputProps={inputProps}
                                     getSuggestionValue={this.onGetSuggestionValue}
                                     suggestions={[""]}
                                     renderSuggestion={this.onRenderSuggestion}
                                     renderInputComponent={this.onRenderInputComponent}
                                     renderSuggestionsContainer={this.onRenderSuggestionsContainer}
                                     onSuggestionsFetchRequested={this.handleSuggestionsFetchRequested}
                                     onSuggestionsClearRequested={this.onSuggestionsClearRequested}/>
                </div>
                <Select style={{marginLeft: "10px"}}
                        value={this.state.selectedCurrencies}
                        onChange={this.onSelectCurrency}
                        renderValue={selectedCurrency}
                        input={<Input id="select-multiple"/>}>
                    {this.generateOptions(this.state.selectedCurrencies, currencyToSelect)}
                </Select>
                <Select style={{marginLeft: "10px"}}
                        value={this.state.selectedCryptocurrency}
                        onChange={this.onSelectCryptocurrency}
                        renderValue={selectedCryptocurrency}
                        input={<Input id="select-multiple"/>}>
                    {this.generateOptions([this.state.selectedCryptocurrency], cryptocurrencyToSelect)}
                </Select>
            </div>
        )
    }

    private onRenderSuggestionsContainer(params: RenderSuggestionsContainerParams): React.ReactNode {
        const result: JSX.Element[] = [];
        for (const suggestion of this.state.suggestions) {
            result.push(
                <MenuItem key={suggestion.appid}
                          value={suggestion.name}
                          onClick={this.onAppClick.bind(this, suggestion)}>
                    <ListItemText primary={suggestion.name}/>
                </MenuItem>
            )
        }
        return (
            <div {...params.containerProps}>
                {result}
            </div>
        );
    }

    private onAppClick(app: SteamApp) {
        Request.getAssets(app, this.state.selectedCurrencies, this.state.selectedCryptocurrency, (r, result) => {
           if (result) {
               this.setState({
                   ...this.state,
                   result
               })
           }
        });
        this.setState({
            ...this.state,
            app
        })
    }

    private onRenderSuggestion(suggestion: string, params: RenderSuggestionParams): JSX.Element {
        window.console.log("onRenderSuggestion", this.state.suggestions);
        return (
            <MenuItem selected={params.isHighlighted} component="div">
                <div>
                    {suggestion}
                </div>
            </MenuItem>
        );
    }

    private onSuggestionsClearRequested() {
        this.setState({
            suggestions: [],
        });
    }

    private onGetSuggestionValue(value: string) {
        return value;
    }

    private onAppChange(event: React.ChangeEvent<HTMLInputElement>, params?: ChangeEvent) {
        this.setState({
            ...this.state,
            appValue: event.target.value
        })
    }

    private handleSuggestionsFetchRequested(request: SuggestionsFetchRequestedParams) {
        Request.getAppsSuggestions(request.value, (r, s) => {
            if (s) {
                this.setState({
                    suggestions: s.suggestions,
                });
            }
        });
    };

    private onRenderInputComponent(inputProps: InputProps<string>): JSX.Element {
        return (
            <TextField
                value={inputProps.value}
                onChange={inputProps.onChange}
            />
        );
    }

    private generateOptions(selection: string[], options?: string[]): JSX.Element[] {
        const result: JSX.Element[] = [];
        if (options) {
            for (const currency of options) {
                result.push(
                    <MenuItem key={currency} value={currency}>
                        <Checkbox checked={selection.indexOf(currency) > -1}/>
                        <ListItemText primary={currency}/>
                    </MenuItem>
                )
            }
        }
        return result
    }

    private onSelectCryptocurrency(event: React.ChangeEvent<HTMLSelectElement>) {
        this.setState({
            ...this.state,
            selectedCryptocurrency: event.target.value
        })
    }

    private onSelectCurrency(event: React.ChangeEvent<HTMLSelectElement>) {
        const index = this.state.selectedCurrencies.indexOf(event.target.value);
        if (index > -1) {
            this.setState({
                ...this.state,
                selectedCryptocurrency: "",
                selectedCurrencies: this.state.selectedCurrencies.filter((v, i) => i !== index)
            })
        } else {
            this.setState({
                ...this.state,
                selectedCurrencies: [
                    ...this.state.selectedCurrencies,
                    event.target.value
                ]
            })
        }
    }
}

export function mapStateToProps(state: AppState, props: ConverterAppProps): ConverterAppProps {
    return {
        ...props,
        currencies: state.currencies
    }
}

function mapDispatchToProps(dispatch: Dispatch<AppAction>, props: ConverterAppProps): ConverterAppProps {
    return {
        ...props,
        onLoadCurrencies: (currencies) => dispatch(Actions.replaceCurrencies(currencies)),
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(ConverterApp);