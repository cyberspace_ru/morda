import * as React from 'react';
import "reflect-metadata"
import * as ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import ConverterApp from "./app/ConverterApp"
import {createStore} from "redux";
import {Provider} from "react-redux";
import {AppState, createInitialState} from "./behaviour/AppState";
import {reduce} from "./behaviour/reducers";
import {Route} from "react-router";
import {HashRouter} from "react-router-dom";

const store = createStore<AppState>(reduce, createInitialState());

ReactDOM.render(
    <Provider store={store}>
        <HashRouter>
            <div>
                <Route exact={true} path="/" component={ConverterApp}/>
            </div>
        </HashRouter>
    </Provider>,
    document.getElementById('root') as HTMLElement);

registerServiceWorker();
