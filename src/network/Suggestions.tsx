import SteamApp from "./SteamApp";
import {Type} from "class-transformer";

export default class Suggestions {
    @Type(() => SteamApp)
    public suggestions: SteamApp[]
}