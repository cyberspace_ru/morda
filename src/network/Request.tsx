import {ClassType} from "class-transformer/ClassTransformer";
import axios, {AxiosError, AxiosResponse} from "axios";
import {plainToClass} from "class-transformer";
import Currencies from "./Currencies";
import Suggestions from "./Suggestions";
import AssetsResult from "./AssetsResult";
import SteamApp from "./SteamApp";

type SuccessCallback<T> = (response: AxiosResponse<string>, t?: T) => void
type FailureCallback = (response: AxiosError) => void

export class Request<T> {
    public static getAssets(app: SteamApp, currencies: string[], cryptocurrency: string, success?: SuccessCallback<AssetsResult>, failure?: FailureCallback) {
        new Request(AssetsResult).get("http://localhost:8080/apps/" + app.appid + "/search?" +
            "currencies=" + currencies.join(",") +
            "&cryptocurrency=" +cryptocurrency +
            "&start=0&limit=900", null, success, failure)
    }

    public static getAppsSuggestions(input: string, success?: SuccessCallback<Suggestions>, failure?: FailureCallback) {
        new Request(Suggestions).get("http://localhost:8100/suggestions/apps?input=" + input, null, success, failure)
    }

    public static getCurrencies(success?: SuccessCallback<Currencies>, failure?: FailureCallback) {
        new Request(Currencies).get("http://localhost:8090/currencies", null, success, failure)
    }

    public readonly returnType: ClassType<T> | undefined;

    constructor(returnType?: ClassType<T>) {
        this.returnType = returnType;
    }

    public get(url: string, data?: any, success?: SuccessCallback<T>, failure?: FailureCallback) {
        axios.get<string>(url, data).then((response) => {
            if (response.status !== 200) {
                window.console.log("error");
            } else if (success) {
                success(response, this.returnType ? plainToClass(this.returnType, response.data) : undefined);
            }
        }).catch(failure);
    }

    public post(url: string, data?: any, success?: SuccessCallback<T>, failure?: FailureCallback) {
        axios.post<string>(url, data).then(response => {
            if (response.status !== 200) {
                window.console.log("error");
            } else if (success) {
                if (this.returnType) {
                    success(response, this.returnType ? plainToClass(this.returnType, response.data, {
                        enableCircularCheck: true
                    }) : undefined);
                } else {
                    success(response, undefined);
                }
            }
        }).catch(failure);
    }
}