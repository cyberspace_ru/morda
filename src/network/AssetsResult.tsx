import Asset from "./Asset";
import {Type} from "class-transformer";

export default class AssetsResult {
    public start: number;
    public limit: number;
    public total: number;
    public appid: number;
    @Type(() => Asset)
    public result: Asset[];
}