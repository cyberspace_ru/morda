import {Type} from "class-transformer";

export default class Asset {
    public classid: string;
    public name: string;
    @Type(() => Map)
    public prices: Map<string, number>;
}