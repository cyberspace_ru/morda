import {Type} from "class-transformer";

export default class Currencies {
    @Type(() => Map)
    public currencies: Map<string, string[]>
}