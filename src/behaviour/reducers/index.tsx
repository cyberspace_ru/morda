import {AppState} from "../AppState";
import {AppAction} from "../actions";
import {COMMON_CHANGED, CURRENCIES_REPLACED} from "../constants";
import {ReplaceCurrencies} from "../actions";

function onCurrenciesReplaced(state: AppState, action: ReplaceCurrencies): AppState {
    return {
        ...state,
        currencies: action.currencies
    };
}

export function reduce(state: AppState, action: AppAction): AppState {
    switch (action.group) {
        case COMMON_CHANGED:
            switch (action.type) {
                case CURRENCIES_REPLACED: {
                    return onCurrenciesReplaced(state, action as ReplaceCurrencies);
                }
            }
            break;
    }
    return state;
}