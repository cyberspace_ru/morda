import Currencies from "../network/Currencies";

export interface AppState {
    currencies?: Currencies
}

export function createInitialState(): AppState {
    return {
        currencies: undefined
    };
}