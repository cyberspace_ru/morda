import {
    COMMON_CHANGED, CURRENCIES_REPLACED
} from "../constants/index";
import Currencies from "../../network/Currencies";

export interface CommonChanges {
    group: COMMON_CHANGED
    type: string
}

export interface ReplaceCurrencies extends CommonChanges {
    currencies: Currencies
}

export type AppAction = CommonChanges;

export class Actions {
    public static replaceCurrencies(currencies: Currencies): ReplaceCurrencies {
        return {
            group: COMMON_CHANGED,
            type: CURRENCIES_REPLACED,
            currencies
        };
    }
}