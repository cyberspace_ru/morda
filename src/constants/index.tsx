import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import {blue} from "@material-ui/core/colors";

export const THEME = createMuiTheme({
    palette: {
        primary: blue,
    },
});